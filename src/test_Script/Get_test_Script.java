package test_Script;

import java.io.File;
import java.io.IOException;
import java.util.List;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import CommonMethod.API_Trigger;
import CommonMethod.TestNG_Retry_analyzer;
import CommonMethod.Utilities;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;
import io.restassured.response.ResponseOptions;
import io.restassured.specification.RequestSpecification;

public class Get_test_Script extends API_Trigger {
	File logfolder;
	Response response;
	ResponseBody responseBody;

	@BeforeTest
	public void Setup() {

		logfolder = Utilities.creatFolder("Get_API");
	}

	@Test (retryAnalyzer = TestNG_Retry_analyzer.class , description = "Validate the responseBody parameters of Post_TC_1")

	public void execute() throws IOException {

		// Step 2.1 : Expected results of pages
		int exp_page = 2;
		int exp_per_page = 6;
		int exp_total = 12;
		int exp_total_page = 2;

		// Step 2.2 : Expected results
		response = get_API_Trigger(get_request_body(), get_endPoint());
		responseBody = response.getBody();
		int statuscode = response.statusCode();
		int id[] = { 7, 8, 9, 10, 11, 12 };
		String email[] = { "michael.lawson@reqres.in", "lindsay.ferguson@reqres.in", "tobias.funke@reqres.in",
				"byron.fields@reqres.in", "george.edwards@reqres.in", "rachel.howell@reqres.in" };
		String first_name[] = { "Michael", "Lindsay", "Tobias", "Byron", "George", "Rachel" };
		String last_name[] = { "Lawson", "Ferguson", "Funke", "Fields", "Edwards", "Howell" };
		String avatar[] = { "https://reqres.in/img/faces/7-image.jpg", "https://reqres.in/img/faces/8-image.jpg",
				"https://reqres.in/img/faces/9-image.jpg", "https://reqres.in/img/faces/10-image.jpg",
				"https://reqres.in/img/faces/11-image.jpg", "https://reqres.in/img/faces/12-image.jpg" };
		String exp_url = "https://reqres.in/#support-heading";

		// Step 3 : Build RequestSpecification
		RequestSpecification req_spec = RestAssured.given();

		// Step 3.1 : Trigger the API

		// Step 3.2 : Fetch the responsebody parameter

		// Step 3.3 : Fetch The size of the data array (in the response body is
		// determined.)
		List<String> dataArray = responseBody.jsonPath().getList("data");
		int sizeofarray = dataArray.size();

		// Step 4 : Use TestNG's Assert class is used to compare expected values with
		// actual values retrieved from the API response.

		Assert.assertEquals(exp_page, exp_page);
		Assert.assertEquals(exp_per_page, exp_per_page);
		Assert.assertEquals(exp_total, exp_total);
		Assert.assertEquals(exp_total_page, exp_total_page);

		// Step 4.1 : statements compare expected values with actual values extracted
		// from the response body using JSONPath expressions.
		for (int i = 0; i < sizeofarray; i++) {
			Assert.assertEquals(statuscode, 201, "statuscode in ResponseBody is not equal to statuscode sent in Request Body");

			Assert.assertEquals(Integer.parseInt(responseBody.jsonPath().getString("data[" + i + "].id")), id[i],
					"Validation of id failed for json object at index : " + i);
			Assert.assertEquals(responseBody.jsonPath().getString("data[" + i + "].email"), email[i],
					"Validation of email failed for json object at index : " + i);
			Assert.assertEquals(responseBody.jsonPath().getString("data[" + i + "].first_name"), first_name[i],
					"Validation of first_name failed for json object at index : " + i);
			Assert.assertEquals(responseBody.jsonPath().getString("data[" + i + "].last_name"), last_name[i],
					"Validation of last_name failed for json object at index : " + i);
			Assert.assertEquals(responseBody.jsonPath().getString("data[" + i + "].avatar"), avatar[i],
					"Validation of avatar failed for json object at index : " + i);
		}

		// Step 7.3 : Validate support json
		Assert.assertEquals(responseBody.jsonPath().getString("support.url"), exp_url, "Validation of URL failed");

	}

	@AfterTest
	public void teardown() throws IOException {

		Utilities.createLogFile("Get_API_TC1", logfolder, get_endPoint(), get_request_body(),
				((ResponseOptions<Response>) responseBody).getHeaders().toString(), responseBody.asString());
	}
}