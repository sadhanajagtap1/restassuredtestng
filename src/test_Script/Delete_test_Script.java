package test_Script;

import java.io.File;
import java.io.IOException;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import CommonMethod.API_Trigger;
import CommonMethod.TestNG_Retry_analyzer;
import CommonMethod.Utilities;
import CommonMethod.Utilities_Delete;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class Delete_test_Script extends API_Trigger {
	File logfolder;
	Response response;
	ResponseBody responseBody;

	@BeforeTest
	public void Setup() {

		logfolder = Utilities.creatFolder("Delete_API");
	}

	@Test (retryAnalyzer = TestNG_Retry_analyzer.class , description = "Validate the responseBody parameters of Post_TC_1")
	public void execute() throws IOException {

		int statuscode = 201;
		for (int i = 0; i < 5; i++) {
			response = delete_API_Trigger(delete_request_body(), delete_endPoint());
			statuscode = response.statusCode();
			System.out.println(statuscode);

			if (statuscode == 201) {
				responseBody = response.getBody();
				System.out.println(responseBody.asString());

				break;
			} else {

				System.out.println("Status Code found in iteration :" + i + " is :" + statuscode
						+ " ,and is not equal expected status code retrying");

			}

		}

		Assert.assertEquals(statuscode, 201, "Correct Status Code not found even after retrying for 5 times");

	}

	@AfterTest
	public void teardown() throws IOException {

		Utilities.createLogFile("Delete_API_TC1", logfolder, delete_request_body(), delete_endPoint(),
				response.getHeaders().toString(), responseBody.asString());

	}
}