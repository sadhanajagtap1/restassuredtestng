package test_Script;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import CommonMethod.API_Trigger;
import CommonMethod.TestNG_Retry_analyzer;
import CommonMethod.Utilities;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class Patch_test_Script extends API_Trigger {
	File logfolder;
	Response response;
	ResponseBody responseBody;

	@BeforeTest
	public void Setup() {

		logfolder = Utilities.creatFolder("Patch_API");
	}

	@Test (retryAnalyzer = TestNG_Retry_analyzer.class , description = "Validate the responseBody parameters of Post_TC_1")

	public void execute() throws IOException {

		response = Patch_API_Trigger(patch_request_body(), patch_endPoint());
		responseBody = response.getBody();
		int statuscode = response.statusCode();
		String res_name = responseBody.jsonPath().getString("name");
		String res_job = responseBody.jsonPath().getString("job");
		String res_updatedAt = responseBody.jsonPath().getString("updatedAt");
		res_updatedAt = res_updatedAt.substring(0, 11);

		JsonPath jsp_req = new JsonPath(patch_request_body());
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);

		Assert.assertEquals(statuscode, 201, "statuscode in ResponseBody is not equal to statuscode sent in Request Body");

		Assert.assertEquals(res_name, req_name, "Name in ResponseBody is not equal to Name sent in Request Body");
		Assert.assertEquals(res_job, req_job, "Job in ResponseBody is not equal to Job sent in Request Body");

		Assert.assertEquals(res_updatedAt, expecteddate, "createdAt in ResponseBody is not equal to Date Generated");

	}

	@AfterTest
	public void teardown() throws IOException {

		Utilities.createLogFile("Patch_API_TC1", logfolder, patch_endPoint(), patch_request_body(),
				response.getHeaders().toString(), responseBody.asString());
	}

}
