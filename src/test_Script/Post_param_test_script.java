package test_Script;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import CommonMethod.API_Trigger;
import CommonMethod.Utilities;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class Post_param_test_script extends API_Trigger {
	File logfolder;
	ResponseBody responseBody;
	Response response;
	Response response1;

	@BeforeTest

	public void Setup() {
		logfolder = Utilities.creatFolder("Post_param_API");
	}

	@Test
	public void validate() throws IOException {
		response = Post_API_Trigger(post_param_requestBody("TC1"), post_endPoint());// extract or create the
																							// request body
		response1 = Post_API_Trigger(post_param_requestBody("TC2"), post_endPoint());// Triggering the api and
																							// sending a request to
																							// requesbody
		 responseBody = response1.getBody();
		int statuscode = response1.statusCode();

		String res_name = responseBody.jsonPath().getString("name");
		String res_job = responseBody.jsonPath().getString("job");
		String res_id = responseBody.jsonPath().getString("id");
		String res_createdAt = responseBody.jsonPath().getString("createdAt");
		res_createdAt = res_createdAt.toString().substring(0, 11);
		// Fetch the request body parameters
		JsonPath jsp_req = new JsonPath(post_param_requestBody("TC1"));
		JsonPath jsp_req1 = new JsonPath(post_param_requestBody("TC2"));

		String req_name = jsp_req1.getString("name");
		String req_job = jsp_req1.getString("job");
		// Generate expected data.
		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);
		Assert.assertEquals(statuscode, 201, "Correct status code not found even after retrying for 6 times.");

		// Validate using TestNG Assertions
		Assert.assertEquals(res_name, req_name, "Name in ResponseBody is not equal to Name sent in Request Body");
		Assert.assertEquals(res_job, req_job, "Job in ResponseBody is not equal to Job sent in Request Body");
		Assert.assertNotNull(res_id, "Id in ResponseBody is found to be null");
		Assert.assertEquals(res_createdAt, expecteddate, "createdAt in ResponseBody is not equal to Date Generated");
	}

	@AfterTest

	public void teardown() throws IOException {
		Utilities.createLogFile("Post_param_test_script", logfolder, post_endPoint(), post_request_body(),
				response1.getHeaders().toString(), responseBody.asString());
	}
}