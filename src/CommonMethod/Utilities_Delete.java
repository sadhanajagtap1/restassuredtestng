package CommonMethod;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

public class Utilities_Delete {

   		public static File creatFolder (String foldername) {
		
	    	//Step 1 : Fetch the current java project
	    	
	    	String  projectFolder = System.getProperty("user.dir");
	    	System.out.println(projectFolder);
	    	 
	    	// Step 2 : check if foldername coming in variabal foldername alrady exists in project folder and create foldername accordingly 
	    	
	    	
	    	File folder = new File(projectFolder+"\\ApiLogs\\"+foldername);
	    	
	    	if(folder.exists()) {
	    		System.out.println(folder + ",Already exists in java project:"+ projectFolder);	
	    		
	    	}
	    	
	    	else {
	    		System.out.println(folder + ",Doesn't  exists in java project:"+ projectFolder+"," + "hence creating it");	
	         folder.mkdir();
	 		System.out.println(folder + ",Created in java project:"+ projectFolder);	
	        	}
	    	
	    	return folder;
	    	
	   }
	    
	    public static void createLogFile  (String Filename, File Filelocation, String endpoint, String responseHeader) throws IOException {
	    	
	    	//Step 1 : 
	    	File newTextFile = new File (Filelocation +"\\" + Filename +".txt");
	    	System.out.println("File create with name:"+newTextFile.getName());
	    	
	    	
	    	//Step 2 : write data into the file
	    	
	    	FileWriter writedata = new FileWriter(newTextFile);
	    	writedata.write("Endpoint is :\n" + endpoint + "\n\n");
	    	//writedata.write("Request body is :\n" + requestBody + "\n\n");
	    	writedata.write("Response header is :\n" + responseHeader + "\n\n");
	    	//writedata.write("Response body is :\n" + resposeBody + "\n\n");
	    	
	    	//Step 3 : Save and close the file 
	    	writedata.close();
	    }
	}


