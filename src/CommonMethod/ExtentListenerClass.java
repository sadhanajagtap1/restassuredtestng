package CommonMethod;

import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.MarkupHelper;
import com.aventstack.extentreports.reporter.ExtentSparkReporter;
import com.aventstack.extentreports.reporter.configuration.Theme;

public class ExtentListenerClass implements ITestListener {

	ExtentSparkReporter sparkReporter; // Line 18: Declaration of ExtentSparkReporter object
	ExtentReports extentReport; // Declaration of ExtentReports object
	ExtentTest test; // Declaration of ExtentTest object

	public void reportConfigurations() { // Method to configure extent report settings
		sparkReporter = new ExtentSparkReporter(".\\extent-report\\report.html"); // Initializing ExtentSparkReporter
																					// object
		extentReport = new ExtentReports(); // Initializing ExtentReports object
		extentReport.attachReporter(sparkReporter); // Attaching ExtentSparkReporter to ExtentReports

		// Adding system/environment information to reports
		extentReport.setSystemInfo("OS", "Windows 11");
		extentReport.setSystemInfo("user", "Vivek");

		// Configurations for changing look & feel of report
		// sparkReporter.config().setDocumentTitle("RestAssured Extent Listener
		// Report");
		sparkReporter.config().setReportName("This is my First Extent-Report");
		sparkReporter.config().setTheme(Theme.DARK);
	}

	// This method will get invoked before start of test case execution. (Same as
	// @BeforeClass Annotation i.e. invokes only once)
	public void onStart(ITestContext result) {
		reportConfigurations();
		System.out.println("On Start method invoked...");
	}

	// This method will get invoked before the start of test case execution.
	public void onFinish(ITestContext result) {
		System.out.println("On Finished method invoked...");
		extentReport.flush();
	}

	// This method will get invoked whenever any of the test cases fail.
	public void onTestFailure(ITestResult result) {
		System.out.println("Name of test method failed: " + result.getName());
		test = extentReport.createTest(result.getName());
		test.log(Status.FAIL,
				MarkupHelper.createLabel("Name of the failed test case is: " + result.getName(), ExtentColor.RED));
	}

	// This method will get invoked whenever any of the test cases is skipped.
	public void onTestSkipped(ITestResult result) {
		System.out.println("Name of test method skipped: " + result.getName());
		test = extentReport.createTest(result.getName());
		test.log(Status.SKIP,
				MarkupHelper.createLabel("Name of the skipped test case is: " + result.getName(), ExtentColor.YELLOW));
	}

	// This method will get invoked on execution of each test case. (Same as
	// (BeforeTest Annotation i.e 4 times)
	public void onTestStart(ITestResult result) {
		System.out.println("Name of test method started: " + result.getName());
	}

	// This method will get invoked whenever any of the test cases pass.
	public void onTestSuccess(ITestResult result) {
		System.out.println("Name of test method executed successfully: " + result.getName());
		test = extentReport.createTest(result.getName());
		test.log(Status.PASS,
				MarkupHelper.createLabel("Name of the passed test case is: " + result.getName(), ExtentColor.GREEN));
	}

	// This method is not being used in the code provided.
	public void onTestFailedButWithinSuccessPercentage(ITestResult result) {
		// No implementation in this example
	}
}