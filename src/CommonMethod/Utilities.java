package CommonMethod;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellType;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class Utilities {

	public static File creatFolder(String foldername) {

		// Step 1 : Fetch the current java project

		String projectFolder = System.getProperty("user.dir");
		System.out.println(projectFolder);

		// Step 2 : check if foldername coming in variabal foldername alrady exists in
		// project folder and create foldername accordingly

		File folder = new File(projectFolder + "\\ApiLogs\\" + foldername);

		if (folder.exists()) {
			System.out.println(folder + ",Already exists in java project:" + projectFolder);

		}

		else {
			System.out
					.println(folder + ",Doesn't  exists in java project:" + projectFolder + "," + "hence creating it");
			folder.mkdir();
			System.out.println(folder + ",Created in java project:" + projectFolder);
		}

		return folder;

	}

	public static void createLogFile(String Filename, File Filelocation, String endpoint, String requestBody,
			String responseHeader, String responseBody) throws IOException {

		// Step 1 :
		File newTextFile = new File(Filelocation + "\\" + Filename + ".txt");
		System.out.println("File create with name:" + newTextFile.getName());

		// Step 2 : write data into the file

		FileWriter writedata = new FileWriter(newTextFile);
		writedata.write("Endpoint is :\n" + endpoint + "\n\n");
		writedata.write("Request body is :\n" + requestBody + "\n\n");
		writedata.write("Response header is :\n" + responseHeader + "\n\n");
		writedata.write("Response body is :\n" + responseBody + "\n\n");

		// Step 3 : Save and close the file
		writedata.close();
	}

	public static ArrayList<String> ReadExcledata(String sheetname, String Testcase) throws IOException {
		ArrayList<String> arrayData = new ArrayList<String>();

		String Projectdir = System.getProperty("user.dir");// thise line find out the location and name of the project
															// and the argument(user.dir)
		FileInputStream fis = new FileInputStream(Projectdir + "\\DataFiles\\InputData.xlsx");// to read from excel file
		XSSFWorkbook wb = new XSSFWorkbook(fis);
		int countofsheets = wb.getNumberOfSheets();
		System.out.println(countofsheets);

		for (int i = 0; i < countofsheets; i++) {

			if (wb.getSheetName(i).equals(sheetname)) {
				System.out.println(wb.getSheetName(i));
				XSSFSheet sheet = wb.getSheetAt(i);
				// iterator is a class in java which help us to moving through iterabal object
				Iterator<Row> rows = sheet.iterator();// row is a chiled of sheet
				while (rows.hasNext()) {// thise method will check till the row having data which we are going to
										// perform the line oprater
					Row datarows = rows.next();// thise method will take our control from one row to next

					String testcasename = datarows.getCell(0).getStringCellValue();// here i need to read the first cell
																					// thats why i use o integer value
					if (testcasename.equals(Testcase)) {
						// to read all cell value of sheet
						Iterator<Cell> cellvalue = datarows.iterator();
						while (cellvalue.hasNext()) {// Iteration: The code enters a while loop with the condition
														// cellvalue.hasNext(). This loop continues as long as there are
														// more cells in the row to be iterated over.
							String testdata = "";// arraylist is string data type and we have decleare globly

							Cell cell = cellvalue.next();// cellvalue.next(), perform some operations on the cell, and
															// then move to the next cell in the row
							CellType datatype = cell.getCellType();// this cell type is going to return the datatype of
																	// cell
							if (datatype.toString().equals("STRING")) {// data type is cell to read/check the data type
								testdata = cell.getStringCellValue();
							} else if (datatype.toString().equals("NUMERIC")) {// here apply else condition to read
																				// numeric value from excel file
								Double num_testdata = cell.getNumericCellValue();// it will give numeric value when in
																					// excel file there is a numeric
																					// value rather than string
								testdata = String.valueOf(num_testdata);// This ensures that the testdata variable
																		// contains a string representation of the
																		// numeric value.
							}

							System.out.println(testdata);

							arrayData.add(testdata);
						}
						break;

					}

				}
				break;

			} else {

				System.out.println("No sheet found of name:" + sheetname + "In current iteration:" + i);
			}

		}
		wb.close();
		return arrayData;
	}

}