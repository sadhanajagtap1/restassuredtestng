package dataDriven_DataProvider;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

import CommonMethod.API_Trigger;
import CommonMethod.TestNG_Retry_analyzer;
import CommonMethod.Utilities;
import io.restassured.path.json.JsonPath;
import io.restassured.response.Response;
import io.restassured.response.ResponseBody;

public class Post_dataprovider_sameclass extends API_Trigger {
	File logfolder;
	Response response;
	ResponseBody responseBody;

	@BeforeTest
	public void Setup() {

		logfolder = Utilities.creatFolder("Post_API");
	}

	@Test(retryAnalyzer = TestNG_Retry_analyzer.class, dataProvider = "requestBody",dataProviderClass = Environmenta_n_repositery.testNG_dataProvider.class, description = "Validate the responseBody parameters of Post_TC_1")

	public void execute(String req_name, String req_job) {
		String requestBody = "{\r\n" + "    \"name\": \"" + req_name + "\",\r\n" + "    \"job\": \"" + req_job
				+ "\"\r\n" + "}";

		response = Post_API_Trigger(requestBody, post_endPoint());

		responseBody = response.getBody();
		System.out.println(responseBody.asString());

		int statuscode = response.statusCode();
		String res_name = responseBody.jsonPath().getString("name");
		String res_job = responseBody.jsonPath().getString("job");
		String res_id = responseBody.jsonPath().getString("id");
		String res_createdAt = responseBody.jsonPath().getString("createdAt");
		res_createdAt = res_createdAt.toString().substring(0, 11);

		JsonPath jsp_req = new JsonPath(post_request_body());
		String Req_name = jsp_req.getString("name");
		String Req_job = jsp_req.getString("job");

		LocalDateTime currentdate = LocalDateTime.now();
		String expecteddate = currentdate.toString().substring(0, 11);

		Assert.assertEquals(statuscode, 201,
				"statuscode in ResponseBody is not equal to statuscode sent in Request Body");
		Assert.assertEquals(res_name, req_name, "Name in ResponseBody is not equal to Name sent in Request Body");
		Assert.assertEquals(res_job, req_job, "Job in ResponseBody is not equal to Job sent in Request Body");
		Assert.assertNotNull(res_id, "Id in ResponseBody is found to be null");
		Assert.assertEquals(res_createdAt, expecteddate, "createdAt in ResponseBody is not equal to Date Generated");

	}

	@AfterTest
	public void teardown() throws IOException {

		Utilities.createLogFile("Post_API_TC1", logfolder, post_endPoint(), post_request_body(),
				response.getHeaders().toString(), responseBody.asString());

	}

}

