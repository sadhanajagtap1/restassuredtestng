package Environmenta_n_repositery;

public class Environmenta {

	public static String post_endPoint() {
		
		String hostname = "https://reqres.in";
		String resource = "/api/users";
		String endPoint = hostname + resource ;
		return endPoint;	
	}
   public static String put_endPoint() {
	   String hostname = "https://reqres.in";
		String resource = "/api/users/2";
		String endPoint = hostname + resource ;
		return endPoint;
   }
   
   public static String patch_endPoint() {
	   String hostname="https://reqres.in";
		String resource="/api/users/2";
		String endPoint = hostname + resource ;
		return endPoint;
   }
 
   public static String get_endPoint() {
	   String hostname = "https://reqres.in";
	   String resource = "/api/users?page=2";
		String endPoint = hostname + resource ;
		return endPoint;
	   
   }
   
   public static String delete_endPoint() {
	   String hostname = " https://reqres.in";
		String resource = "/api/users/2";
		String endPoint = hostname + resource ;
		return endPoint;
   }
	   
 
}
   