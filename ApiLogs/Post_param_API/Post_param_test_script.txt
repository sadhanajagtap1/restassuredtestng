Endpoint is :
https://reqres.in/api/users

Request body is :
{
    "name": "morpheus",
    "job": "leader"
}

Response header is :
Date=Tue, 07 May 2024 12:35:45 GMT
Content-Type=application/json; charset=utf-8
Content-Length=80
Connection=keep-alive
Report-To={"group":"heroku-nel","max_age":3600,"endpoints":[{"url":"https://nel.heroku.com/reports?ts=1715085345&sid=c4c9725f-1ab0-44d8-820f-430df2718e11&s=zb1TAcFQR30ZjLUUHu4x6Y1yMjAkuWhKLELJZDTBIQk%3D"}]}
Reporting-Endpoints=heroku-nel=https://nel.heroku.com/reports?ts=1715085345&sid=c4c9725f-1ab0-44d8-820f-430df2718e11&s=zb1TAcFQR30ZjLUUHu4x6Y1yMjAkuWhKLELJZDTBIQk%3D
Nel={"report_to":"heroku-nel","max_age":3600,"success_fraction":0.005,"failure_fraction":0.05,"response_headers":["Via"]}
X-Powered-By=Express
Access-Control-Allow-Origin=*
Etag=W/"50-kRNLklt7kk3V28OKuqrfH/ckGok"
Via=1.1 vegur
CF-Cache-Status=DYNAMIC
Server=cloudflare
CF-RAY=8801406debc99fb9-SIN

Response body is :
{"name":"2.0","job":"Testlead","id":"95","createdAt":"2024-05-07T12:35:45.316Z"}

